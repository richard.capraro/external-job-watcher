FROM openjdk:11-jdk-slim

RUN mkdir /app

COPY build/libs/external-job-watcher-0.0.1-all.jar /app/external-job-watcher.jar
WORKDIR /app

CMD ["java", "-jar", "external-job-watcher.jar"]
