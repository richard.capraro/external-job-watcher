package io.saagie

import io.fabric8.kubernetes.api.model.PodBuilder
import io.fabric8.kubernetes.client.server.mock.KubernetesServer
import java.util.UUID

const val namespace = "external-jobs"
const val podName = "ejw"

fun KubernetesServer.createTestPod(jobExecutionId: String) {

    val pod = PodBuilder()
        .withNewMetadata()
        .withNewUid(UUID.randomUUID().toString())
        .withNewName(podName)
        .withNewNamespace(namespace)
        .withLabels(mapOf(LABEL_JOB_EXECUTION_ID_KEY to jobExecutionId))
        .endMetadata()
        .build()

    this.client.inNamespace(namespace).pods().create(pod)
}
