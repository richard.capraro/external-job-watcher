package io.saagie

import io.fabric8.kubernetes.client.server.mock.KubernetesServer
import io.kotest.core.spec.DisplayName
import io.kotest.core.spec.style.FunSpec
import io.kotest.extensions.system.SpecSystemExitListener
import org.slf4j.bridge.SLF4JBridgeHandler

@DisplayName("Test Kubernetes Helper")
class KubernetesHelperTest : FunSpec() {

    private var mockK8sServer: KubernetesServer = KubernetesServer(false, true)

    init {
        SLF4JBridgeHandler.removeHandlersForRootLogger()
        SLF4JBridgeHandler.install()
        listeners(mockK8sServer.perTest(), SpecSystemExitListener)

        context("Update ExternalStatus Label") {

            test("success") {
            }

            test("failed") {
            }
        }
    }
}
