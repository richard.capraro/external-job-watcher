package io.saagie

import io.fabric8.kubernetes.client.server.mock.KubernetesServer
import io.kotest.core.listeners.TestListener
import io.kotest.core.test.TestCase
import io.kotest.core.test.TestResult

fun KubernetesServer.perTest() = MockK8sServerPerTestListener(this)

class MockK8sServerPerTestListener(private val mockK8sServer: KubernetesServer) : TestListener {

    override suspend fun beforeTest(testCase: TestCase) {
        mockK8sServer.before()
    }

    override suspend fun afterTest(testCase: TestCase, result: TestResult) {
        mockK8sServer.after()
    }
}
