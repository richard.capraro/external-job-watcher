package io.saagie

import mu.KotlinLogging
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.wait.strategy.Wait

private val logger = KotlinLogging.logger("ETR")

class EtrFakeContainer(dockerImageName: String) : GenericContainer<EtrFakeContainer>(dockerImageName) {

    override fun start() {
        logger.info { "Starting container..." }
        super.start()
        followOutput { frame -> logger.info { frame.utf8String } }
    }

    override fun stop() {
        logger.info { "Stopping container..." }
        super.stop()
    }

    companion object {
        operator fun invoke(version: String = "latest"): EtrFakeContainer =
            EtrFakeContainer("saagie/external-technology-runner-fake:$version")
                .withExposedPorts(8000)
                .waitingFor(Wait.forListeningPort())
    }
}
