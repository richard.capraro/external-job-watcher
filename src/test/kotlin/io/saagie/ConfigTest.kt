package io.saagie

import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.kotest.assertions.fail
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.FunSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.extensions.system.withEnvironment
import io.kotest.matchers.shouldBe
import io.saagie.Config.Companion.load
import io.saagie.EnvVars.EJW_ETR_CONNECTION_TIMEOUT
import io.saagie.EnvVars.EJW_ETR_REQUEST_TIMEOUT
import io.saagie.EnvVars.EJW_ETR_URL
import io.saagie.EnvVars.EJW_ETR_VERIFY_SSL
import io.saagie.EnvVars.EJW_NAMESPACE
import io.saagie.EnvVars.EJW_PAUSE_DURATION
import io.saagie.EnvVars.EJW_SCENARIO
import io.saagie.EnvVars.EJW_START_FUNCTION
import io.saagie.EnvVars.EJW_START_SCRIPT
import io.saagie.EnvVars.EJW_STATUS_FUNCTION
import io.saagie.EnvVars.EJW_STATUS_SCRIPT
import io.saagie.EnvVars.EJW_UUID

typealias ConfigMap = Map<String, String>

class ConfigTest : FunSpec() {

    init {
        context("EnvVars") {

            val envVarValue = "plop"

            test("invoke") {
                withEnvironment((EJW_UUID.name to envVarValue).toEnv()) {
                    EJW_UUID() shouldBe envVarValue
                    EJW_ETR_URL() shouldBe null
                }
            }
            test("get") {
                withEnvironment((EJW_UUID.name to envVarValue).toEnv()) {
                    EJW_UUID.get() shouldBe envVarValue
                    val exception = shouldThrow<IllegalStateException> {
                        EJW_ETR_URL.get()
                    }
                    exception.message shouldBe "Env var $EJW_ETR_URL is null or empty"
                }
            }
            test("getLongOrDefault") {
                val connectionTimeoutValue = 500L
                val defaultValue = 42L
                withEnvironment(
                    listOf(
                        EJW_ETR_CONNECTION_TIMEOUT.name to connectionTimeoutValue.toString(),
                        EJW_ETR_REQUEST_TIMEOUT.name to "pwet"
                    ).toEnv()
                ) {
                    EJW_ETR_CONNECTION_TIMEOUT.getLongOrDefault(defaultValue) shouldBe connectionTimeoutValue
                    EJW_ETR_REQUEST_TIMEOUT.getLongOrDefault(defaultValue) shouldBe defaultValue
                    EJW_PAUSE_DURATION.getLongOrDefault(defaultValue) shouldBe defaultValue
                }
            }

            test("isEnabled") {
                forAll(
                    row("true", true),
                    row("false", false),
                    row("yes", true),
                    row("no", false),
                    row("1", true),
                    row("0", false),
                    row("42", false)
                ) { value, result ->
                    withEnvironment((EJW_ETR_VERIFY_SSL.name to value).toEnv()) {
                        EJW_ETR_VERIFY_SSL.isEnabled() shouldBe result
                    }
                }
            }
        }

        context("Config") {
            test("load with a jobInstanceId") {
                val jsonEnvsFile = javaClass.classLoader.getResourceAsStream("config_test.json")!!
                val configMap = jacksonObjectMapper()
                    .registerModule(KotlinModule())
                    .readValue<ConfigMap>(jsonEnvsFile)

                withEnvironment(configMap) {
                    configMap.test()
                }
            }

            test("load without a jobInstanceId") {
                val jsonEnvsFile = javaClass.classLoader.getResourceAsStream("config_test.json")!!
                val configMap = jacksonObjectMapper()
                    .registerModule(KotlinModule())
                    .readValue<ConfigMap>(jsonEnvsFile)
                    .toMutableMap()

                configMap.remove(EJW_UUID.name)

                withEnvironment(configMap) {
                    configMap.test()
                    load().instance.uuid shouldBe null
                }
            }
        }
    }
}

private fun ConfigMap.test() {
    load().etrClient.url shouldBe
        this.getEnVarOrFail(EJW_ETR_URL)
    load().etrClient.requestTimeoutInMs shouldBe
        this.getEnVarOrFail(EJW_ETR_REQUEST_TIMEOUT).toLong()
    load().etrClient.connectTimeoutInMs shouldBe
        this.getEnVarOrFail(EJW_ETR_CONNECTION_TIMEOUT).toLong()
    load().etrClient.verifySSL shouldBe
        this.getEnVarOrFail(EJW_ETR_VERIFY_SSL).toBoolean()

    load().instance.name shouldBe
        this.getEnVarOrFail(EnvVars.EJW_NAME)
    load().instance.namespace shouldBe
        this.getEnVarOrFail(EJW_NAMESPACE)
    load().instance.start.function shouldBe
        this.getEnVarOrFail(EJW_START_FUNCTION)
    load().instance.start.script shouldBe
        this.getEnVarOrFail(EJW_START_SCRIPT)
    load().instance.status.function shouldBe
        this.getEnVarOrFail(EJW_STATUS_FUNCTION)
    load().instance.status.script shouldBe
        this.getEnVarOrFail(EJW_STATUS_SCRIPT)

    load().scenario shouldBe
        this.getEnVarOrFail(EJW_SCENARIO)

    load().pauseDurationInMs shouldBe
        this.getEnVarOrFail(EJW_PAUSE_DURATION).toLong()
}

private fun ConfigMap.getEnVarOrFail(envVar: EnvVars) =
    this.getOrElse(envVar.name) { fail("$envVar not found in environment variables") }

private fun Pair<String, String>.toEnv(): Map<String, String> = mapOf(this)
private fun List<Pair<String, String>>.toEnv(): Map<String, String> = this.toMap()
