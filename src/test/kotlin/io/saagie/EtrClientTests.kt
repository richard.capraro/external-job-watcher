package io.saagie

import io.fabric8.kubernetes.client.server.mock.KubernetesServer
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.DisplayName
import io.kotest.core.spec.style.FunSpec
import io.kotest.extensions.system.SpecSystemExitListener
import io.kotest.extensions.system.SystemExitException
import io.kotest.extensions.system.withEnvironment
import io.kotest.extensions.testcontainers.perSpec
import io.kotest.matchers.maps.shouldContain
import io.kotest.matchers.shouldBe
import io.saagie.EnvVars.EJW_ETR_URL
import io.saagie.EnvVars.EJW_NAMESPACE
import io.saagie.EnvVars.EJW_START_FUNCTION
import io.saagie.EnvVars.EJW_START_SCRIPT
import io.saagie.EnvVars.EJW_STATUS_FUNCTION
import io.saagie.EnvVars.EJW_STATUS_SCRIPT
import io.saagie.EnvVars.EJW_UUID
import io.saagie.ExitCode.`🥳`
import mu.KotlinLogging
import org.slf4j.bridge.SLF4JBridgeHandler
import java.util.UUID

private val logger = KotlinLogging.logger {}

@DisplayName("Test ETR HTTP client")
internal class EtrClientTests : FunSpec() {

    private val etrContainer = EtrFakeContainer()
    private var mockK8sServer: KubernetesServer = KubernetesServer(false, true)

    private fun createEnvironment(jobExecutionId: String): Map<String, String> =
        createEnvironment() + (EJW_UUID.name to jobExecutionId)

    private fun createEnvironment(): Map<String, String> = mapOf(
        EJW_ETR_URL.name to etrContainer.toEtrUrl(),
        EnvVars.EJW_NAME.name to podName,
        EJW_NAMESPACE.name to namespace,
        EJW_START_SCRIPT.name to "foo",
        EJW_START_FUNCTION.name to "start",
        EJW_STATUS_SCRIPT.name to "bar",
        EJW_STATUS_FUNCTION.name to "getStatus",
    )

    private fun EtrFakeContainer.toEtrUrl(): String = "http://$containerIpAddress:$firstMappedPort/api/execute"

    init {
        SLF4JBridgeHandler.removeHandlersForRootLogger()
        SLF4JBridgeHandler.install()

        listeners(etrContainer.perSpec(), mockK8sServer.perTest(), SpecSystemExitListener)

        context("Call ETR api") {

            test("call ETR start with a job that will succeed") {
                val successUuid = "c0ffee01-0002-0004-0002-c0ffeec0ffee"
                mockK8sServer.createTestPod(successUuid)
                withEnvironment(createEnvironment(successUuid)) {
                    logger.info { System.getenv() }
                    val kubernetesHelper = KubernetesHelper(mockK8sServer.client)
                    val watcher = Watcher(kubernetesHelper = kubernetesHelper)
                    watcher.startJob() shouldBe true
                    shouldThrow<SystemExitException> {
                        watcher.checkIfJobIsNotFinished()
                    }.exitCode shouldBe `🥳`.code
                    val updatedPod = mockK8sServer
                        .client
                        .pods()
                        .inNamespace(EJW_NAMESPACE.get())
                        .withName(podName)
                        .get()
                    updatedPod.metadata.labels shouldContain (LABEL_EXTERNAL_STATUS_KEY to "SUCCEEDED")
                }
            }

            test("call ETR start with a job that will fail") {
                val failedUuid = "c0ffee00-0002-0004-0002-c0ffeec0ffee"
                withEnvironment(createEnvironment(failedUuid)) {
                    logger.info { System.getenv() }
                    mockK8sServer.createTestPod(failedUuid)
                    val kubernetesHelper = KubernetesHelper(mockK8sServer.client)
                    val watcher = Watcher(kubernetesHelper = kubernetesHelper)
                    watcher.startJob() shouldBe true
                    shouldThrow<SystemExitException> {
                        watcher.checkIfJobIsNotFinished()
                    }.exitCode shouldBe ExitCode.`🙁`.code
                    val updatedPod = mockK8sServer
                        .client
                        .pods()
                        .inNamespace(EJW_NAMESPACE.get())
                        .withName(podName)
                        .get()
                    updatedPod.metadata.labels shouldContain (LABEL_EXTERNAL_STATUS_KEY to "FAILED")
                }
            }

            test("call ETR with a failing start") {
                val uuid = UUID.randomUUID().toString()
                val environment = createEnvironment(uuid)
                    .toMutableMap()
                    .apply {
                        this[EJW_ETR_URL.name] = "https://httpbin.org/status/500"
                    }
                withEnvironment(environment) {
                    logger.info { System.getenv() }
                    val watcher = Watcher()
                    watcher.startJob() shouldBe false
                }
            }

            test("call ETR without an existing jobExecutionId") {
                val environment = createEnvironment()
                withEnvironment(environment) {
                    logger.info { System.getenv() }
                    val watcher = Watcher()
                    watcher.startJob() shouldBe true
                }
            }
        }
    }
}
