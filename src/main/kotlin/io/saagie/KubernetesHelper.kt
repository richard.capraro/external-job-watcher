package io.saagie

import io.fabric8.kubernetes.api.model.Pod
import io.fabric8.kubernetes.client.DefaultKubernetesClient
import io.fabric8.kubernetes.client.NamespacedKubernetesClient
import mu.KotlinLogging
import java.util.UUID

internal const val LABEL_EXTERNAL_STATUS_KEY = "io.saagie/external-status"
internal const val LABEL_JOB_EXECUTION_ID_KEY = "io.saagie/job-execution-id"

private val logger = KotlinLogging.logger {}

public class KubernetesHelper(private val client: NamespacedKubernetesClient = DefaultKubernetesClient()) {

    public fun updateExternalStatusLabel(watcher: Watcher, value: String) {
        val pod = getMyself(watcher)

        if (pod.metadata.labels[LABEL_EXTERNAL_STATUS_KEY] != value) {
            pod.metadata.labels[LABEL_EXTERNAL_STATUS_KEY] = value
            savePod(pod)
        }
    }

    public fun setJobExecutionIdIfNotExist(watcher: Watcher) {
        val pod = getMyself(watcher)
        if (pod.metadata.labels[LABEL_JOB_EXECUTION_ID_KEY].isNullOrBlank()) {
            pod.metadata.labels[LABEL_JOB_EXECUTION_ID_KEY] = pod.metadata.uid
            logger.info { "❌ $LABEL_JOB_EXECUTION_ID_KEY not present, updating the pod to set it to ${pod.metadata.uid}" }
            watcher.config.instance.uuid = UUID.fromString(pod.metadata.uid)
            savePod(pod)
        } else {
            logger.info { "✅ $LABEL_JOB_EXECUTION_ID_KEY already present" }
        }
    }

    private fun getMyself(watcher: Watcher): Pod = client
        .inNamespace(watcher.config.instance.namespace)
        .pods()
        .withName(watcher.config.instance.name)
        .get()
        ?: throw IllegalStateException("Pod with name $watcher.config.instance.name' does not exist in '$watcher.config.instance.name namespace'")

    private fun savePod(pod: Pod) {
        client
            .inNamespace(pod.metadata.namespace)
            .pods()
            .withName(pod.metadata.name)
            .patch(pod)
    }
}
