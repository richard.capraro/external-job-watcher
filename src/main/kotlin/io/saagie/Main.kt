package io.saagie

import io.saagie.ExitCode.`🤕`
import io.saagie.ExitCode.`🤬`
import mu.KotlinLogging
import kotlin.system.exitProcess

private val logger = KotlinLogging.logger {}

public suspend fun main() {
    try {
        with(Watcher()) {
            setJobExecutionIdIfNotExist()
            if (startJob()) {
                logger.info { "Wait until job is finished" }
                checkIfJobIsNotFinished()
            } else {
                logger.warn { "Job has not been started, exiting..." }
                exitProcess(`🤕`) // fail
            }
        }
    } catch (e: Exception) {
        logger.error(e) { "🚨 Unexpected error 🚨" }
        logger.warn {
            val envVars = System.getenv()
                .filterKeys { it.startsWith("EJW") }
                .toList()
                .joinToString("\n") { (key, value) -> "$key=$value" }
                .prependIndent("  ")
            "EnvVars: \n$envVars"
        }
        exitProcess(`🤬`)
    }
}

public fun exitProcess(exitCode: ExitCode) {
    logger.info { "👋 $exitCode" }
    exitProcess(exitCode.code)
}

public enum class ExitCode(public val code: Int, private val description: String) {
    `🥳`(0, "EXTERNAL_JOB_EXECUTION_SUCCESS"),
    `🙁`(1, "EXTERNAL_JOB_EXECUTION_FAILED"),
    `🤕`(2, "EXTERNAL_JOB_EXECUTION_FAIL_TO_START"),
    `🤬`(128, "PANIC");

    override fun toString(): String = "$name - ($code) $description"
}
