package io.saagie

import io.saagie.EnvVars.EJW_ETR_CONNECTION_TIMEOUT
import io.saagie.EnvVars.EJW_ETR_REQUEST_TIMEOUT
import io.saagie.EnvVars.EJW_ETR_URL
import io.saagie.EnvVars.EJW_ETR_VERIFY_SSL
import io.saagie.EnvVars.EJW_NAME
import io.saagie.EnvVars.EJW_NAMESPACE
import io.saagie.EnvVars.EJW_PAUSE_DURATION
import io.saagie.EnvVars.EJW_SCENARIO
import io.saagie.EnvVars.EJW_START_FUNCTION
import io.saagie.EnvVars.EJW_START_SCRIPT
import io.saagie.EnvVars.EJW_STATUS_FUNCTION
import io.saagie.EnvVars.EJW_STATUS_SCRIPT
import io.saagie.EnvVars.EJW_UUID
import mu.KotlinLogging
import java.util.UUID

private val logger = KotlinLogging.logger {}

public enum class EnvVars {
    EJW_UUID,
    EJW_NAME,
    EJW_SCENARIO,
    EJW_NAMESPACE,
    EJW_ETR_URL,
    EJW_ETR_CONNECTION_TIMEOUT,
    EJW_ETR_REQUEST_TIMEOUT,
    EJW_ETR_VERIFY_SSL,
    EJW_START_SCRIPT,
    EJW_START_FUNCTION,
    EJW_STATUS_SCRIPT,
    EJW_STATUS_FUNCTION,
    EJW_PAUSE_DURATION;

    public operator fun invoke(): String? =
        System.getenv(this.name)

    public fun get(): String =
        this() ?: throw IllegalStateException("Env var $name is null or empty")

    public fun <T> getOrDefault(transform: (String) -> T, default: () -> T): T =
        this()?.let {
            try {
                transform(it)
            } catch (e: Exception) {
                default()
            }
        } ?: default()

    public fun getLongOrDefault(value: Long): Long =
        getOrDefault(String::toLong) { value }

    public fun isEnabled(): Boolean =
        when (this()) {
            "true", "yes", "1" -> true
            else -> false
        }
}

public data class Config(
    val etrClient: EtrClientConfig,
    val instance: InstanceConfig,
    val scenario: String?,
    val pauseDurationInMs: Long
) {

    internal companion object {
        fun load(): Config =
            Config(
                etrClient = EtrClientConfig.load(),
                instance = InstanceConfig.load(),
                scenario = EJW_SCENARIO(),
                pauseDurationInMs = EJW_PAUSE_DURATION.getLongOrDefault(3000), // 3s
            )
    }
}

public data class EtrClientConfig(
    val url: String,
    val connectTimeoutInMs: Long,
    val requestTimeoutInMs: Long,
    val verifySSL: Boolean,
) {

    internal companion object {
        fun load(): EtrClientConfig =
            EtrClientConfig(
                url = EJW_ETR_URL.get(),
                connectTimeoutInMs = EJW_ETR_CONNECTION_TIMEOUT.getLongOrDefault(1000), // 1s
                requestTimeoutInMs = EJW_ETR_REQUEST_TIMEOUT.getLongOrDefault(1000), // 1s
                verifySSL = EJW_ETR_VERIFY_SSL.isEnabled()
            )
    }
}

public data class InstanceConfig(
    var uuid: UUID?,
    val name: String,
    val namespace: String,
    val start: Start,
    val status: Status
) {

    internal companion object {
        fun load(): InstanceConfig =
            InstanceConfig(
                uuid = EJW_UUID.getOrDefault({ UUID.fromString(it) }) { null },
                name = EJW_NAME.get(),
                namespace = EJW_NAMESPACE.get(),
                start = Start.load(),
                status = Status.load()
            )
    }
}

public data class Start(val script: String, val function: String) {

    internal companion object {
        fun load(): Start =
            Start(
                script = EJW_START_SCRIPT.get(),
                function = EJW_START_FUNCTION.get(),
            )
    }
}

public data class Status(val script: String, val function: String) {

    internal companion object {
        fun load(): Status =
            Status(
                script = EJW_STATUS_SCRIPT.get(),
                function = EJW_STATUS_FUNCTION.get(),
            )
    }
}
