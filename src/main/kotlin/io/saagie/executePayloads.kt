package io.saagie

import java.util.UUID

public data class ExecuteRequestPayload(
    val script: String,
    val function: String,
    val uuid: UUID,
    val params: ExecuteRequestPayloadParams
)

public data class ExecuteRequestPayloadParams(val featuresValues: Map<String, Any> = emptyMap())

public fun Watcher.startExecuteRequestPayload(): ExecuteRequestPayload = ExecuteRequestPayload(
    script = this.config.instance.start.script,
    function = this.config.instance.start.function,
    uuid = if (this.config.scenario != null) UUID.fromString(this.config.scenario) else this.config.instance.uuid!!,
    params = ExecuteRequestPayloadParams()
)

public fun Watcher.statusExecuteRequestPayload(): ExecuteRequestPayload = ExecuteRequestPayload(
    script = this.config.instance.status.script,
    function = this.config.instance.status.function,
    uuid = if (this.config.scenario != null) UUID.fromString(this.config.scenario) else this.config.instance.uuid!!,
    params = ExecuteRequestPayloadParams()
)

public data class ExecuteResponsePayload(val data: String?, val status: String)
