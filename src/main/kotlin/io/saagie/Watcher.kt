package io.saagie

import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
import io.ktor.client.features.HttpTimeout
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.logging.DEFAULT
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logger
import io.ktor.client.features.logging.Logging
import io.ktor.client.request.post
import io.ktor.client.request.url
import io.ktor.client.statement.HttpResponse
import io.ktor.http.ContentType.Application.Json
import io.ktor.http.HttpStatusCode
import io.ktor.http.contentType
import io.saagie.ExitCode.`🙁`
import io.saagie.ExitCode.`🥳`
import kotlinx.coroutines.delay
import mu.KotlinLogging
import org.apache.http.conn.ssl.NoopHostnameVerifier
import org.apache.http.conn.ssl.TrustSelfSignedStrategy
import org.apache.http.ssl.SSLContextBuilder

private val logger = KotlinLogging.logger {}

public data class Watcher(
    val config: Config = Config.load(),
    val kubernetesHelper: KubernetesHelper = KubernetesHelper(),
) {

    private val httpClient: HttpClient by lazy {
        logger.debug { "Create ETR HTTP client with config ${config.etrClient}" }
        HttpClient(Apache) {
            install(HttpTimeout) {
                connectTimeoutMillis = config.etrClient.connectTimeoutInMs
                requestTimeoutMillis = config.etrClient.requestTimeoutInMs
            }
            install(JsonFeature) {
                serializer = JacksonSerializer()
            }
            install(Logging) {
                logger = Logger.DEFAULT
                level = LogLevel.ALL
            }
            engine {
                customizeClient {
                    if (!config.etrClient.verifySSL) {
                        logger.info { "⚠️ Allowing SSL self signed certificates" }
                        val sslContext = SSLContextBuilder
                            .create()
                            .loadTrustMaterial(TrustSelfSignedStrategy())
                            .build()
                        setSSLContext(sslContext)
                        setSSLHostnameVerifier(NoopHostnameVerifier())
                    }
                }
            }
        }
    }

    private fun updateExternalStatusLabel(externalJobStatus: String) {
        logger.info { "Updating status to $externalJobStatus" }
        kubernetesHelper.updateExternalStatusLabel(this, externalJobStatus)
    }

    public fun setJobExecutionIdIfNotExist() {
        logger.info { "❔Checking the presence of $LABEL_JOB_EXECUTION_ID_KEY in the pod labels..." }
        kubernetesHelper.setJobExecutionIdIfNotExist(this)
    }

    public suspend fun checkIfJobIsNotFinished() {
        logger.info { "Check and wait until job is finished" }

        while (true) {
            val externalJobStatus = retrieveExternalJobStatus()
            if (externalJobStatus != null) {
                updateExternalStatusLabel(externalJobStatus)
            }
            when (externalJobStatus) {
                "SUCCEEDED" -> exitProcess(`🥳`) // Should exit
                "FAILED", "KILLED" -> exitProcess(`🙁`) // Should exit
                else -> logger.debug { "Not yet finished, retry in ${config.pauseDurationInMs}ms" }
            }
            delay(config.pauseDurationInMs)
        }
    }

    private suspend fun retrieveExternalJobStatus(): String? =
        try {
            logger.debug { "Retrieve external job status" }
            val executeResponsePayload = httpClient.post<ExecuteResponsePayload> {
                url(config.etrClient.url)
                contentType(Json)
                body = statusExecuteRequestPayload()
            }
            executeResponsePayload.data
        } catch (e: Throwable) {
            logger.warn(e) { "Impossible to retrieve external job status, retry in ${config.pauseDurationInMs}ms ⌛" }
            null
        }

    public suspend fun startJob(): Boolean =
        try {
            logger.info { "Starting Job" }
            val response = httpClient.post<HttpResponse> {
                url(config.etrClient.url)
                contentType(Json)
                body = startExecuteRequestPayload()
            }

            if (response.status == HttpStatusCode.OK) {
                logger.debug { "Job started" }
                true
            } else {
                logger.error { "Job cannot be started\n$response" }
                false
            }
        } catch (t: Throwable) {
            logger.error(t) { "Job cannot be started" }
            false
        }
}
