import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jlleitschuh.gradle.ktlint.reporter.ReporterType
import org.owasp.dependencycheck.reporting.ReportGenerator

val ktor_version: String by project
val fabric8_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val kotest_version: String by project
val testcontainers_version: String by project
val kotlinlogging_version: String by project
val slf4j_version: String by project

plugins {
    application
    kotlin("jvm") version "1.4.0"
    id("org.jlleitschuh.gradle.ktlint") version "9.3.0"
    id("org.owasp.dependencycheck") version "5.3.2.1"
    id("com.adarshr.test-logger") version "2.1.0"
    id("name.remal.check-updates") version "1.0.211"
    id("com.github.johnrengelman.shadow") version "6.0.0"
}

group = "io.saagie"
version = "0.0.1"

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions.suppressWarnings = true
    kotlinOptions.jvmTarget = "11"
}

application {
    mainClassName = "io.saagie.MainKt"
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<Jar> {
    manifest {
        attributes(
            mapOf(
                "Main-Class" to application.mainClassName
            )
        )
    }
}

kotlin {
    explicitApi()
}

ktlint {
    version.set("0.38.1")
    debug.set(false)
    verbose.set(false)
    reporters {
        reporter(ReporterType.PLAIN)
        reporter(ReporterType.CHECKSTYLE)
    }
}

dependencyCheck {
    format = ReportGenerator.Format.ALL
    suppressionFile = "ignore-cve.xml"
    outputDirectory = "target"
}

testlogger {
    showFullStackTraces = true
}

repositories {
    mavenLocal()
    jcenter()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version")
    runtimeOnly("ch.qos.logback:logback-classic:$logback_version")
    implementation("io.github.microutils:kotlin-logging:$kotlinlogging_version")
    implementation("io.ktor:ktor-client-core:$ktor_version")
    implementation("io.ktor:ktor-client-core-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-apache:$ktor_version")
    implementation("io.ktor:ktor-client-json-jvm:$ktor_version")
    implementation("io.ktor:ktor-client-jackson:$ktor_version")
    implementation("io.ktor:ktor-client-logging-jvm:$ktor_version")
    implementation("io.fabric8:kubernetes-client:$fabric8_version")

    testImplementation("io.fabric8:kubernetes-server-mock:$fabric8_version")
    testImplementation("io.ktor:ktor-client-mock:$ktor_version")
    testImplementation("io.ktor:ktor-client-mock-jvm:$ktor_version")
    testImplementation("io.kotest:kotest-runner-junit5-jvm:$kotest_version")
    testImplementation("io.kotest:kotest-extensions-testcontainers:$kotest_version")
    testImplementation("org.testcontainers:testcontainers:$testcontainers_version")
    testImplementation("org.slf4j:jul-to-slf4j:$slf4j_version")
}
