#!/usr/bin/env bash

set -e

echo "📦 Compile and genererate jar ..."
./gradlew shadowJar

echo "🐳 building docker image..."
docker build -t saagie/external-job-watcher .
